## Protocol specification

Send format: `CMD_ID;[DATA, if need]`

### commands:
**0 - get all snakes**, return `[x],[y],[RRGGBB],[BODY];[x2],[y2],[RRGGBB],[BODY2];...`

The BODY is represented as an array of coordinates of this part of the body separated by the symbol `,` (for example: `x,y,x2,y2,x3,y3, ...`) 

Full example: `110,53,CCCCCC,110,52,110,51,109,51;85,67,BBAAFF,85,66,84,66; ...`

**1 - change snake direction**. DATA - direction: `0 - top, 1 - down, 3 - right, 4 - left`

**2 - get all blocks**, return array of blocks: `[x],[y],[RRGGBB];...`