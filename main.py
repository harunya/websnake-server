import asyncio
import websockets
import random
import threading
import time

players = {}
blocks = []

map_size_x = 30
map_size_y = 30


def parse_cmd(cmd):
    r = cmd.split(";")
    return {"cmd_id": r[0], "data": r[1:]}


def build_cmd(cmd_id, args=None):
    if args is None:
        args = []

    return ";".join([str(cmd_id)] + args)


def gen_hex_color(r, g, b):
    def hexx(c):
        return ("0" + hex(int(c)).replace("0x", ""))[-2:]

    return "{}{}{}".format(hexx(r), hexx(g), hexx(b))


def rnd_clr():
    return random.randint(0, 255)


def create_snake(key, x=None, y=None):
    x = random.randint(1, map_size_x - 2) if x is None else x
    y = random.randint(1, map_size_y - 2) if y is None else y

    if key in players:
        return 0

    players[key] = [x, y, gen_hex_color(rnd_clr(), rnd_clr(), rnd_clr()), [], random.randint(0, 3)]

    add_snake_length(key, 10)

    return 1


def add_snake_length(key, count=1):
    for _ in range(count):
        p = players[key]
        dd = ([[p[0], p[1]]] + p[3])

        p[3].append(nnssd(dd[-1][0], dd[-1][1], p[4]))


def process_cmd(cmd, id):
    args = parse_cmd(cmd)

    if not args["cmd_id"].isnumeric():
        return build_cmd(-1, args["data"])

    cmd_id = int(args["cmd_id"])

    if cmd_id == 0:
        s = []
        for pn in players:
            p = players[pn]
            # print(p[3])
            s.append(",".join(list(map(str, p[0:3])) + list(map(lambda d: ",".join(map(str, d)), p[3]))))

        return build_cmd(0, s)

    if cmd_id == 1:
        if not args["data"][0].isnumeric() or int(args["data"][0]) not in range(0, 4):
            return build_cmd(-1)

        players[id][4] = int(args["data"][0])

    if cmd_id == 2:
        return build_cmd(2, list(map(lambda x: ",".join(map(str, x)), blocks)))

    if cmd_id == 9999:  # for debug
        return build_cmd(-1, [id])

    return build_cmd(args["cmd_id"], args["data"])


async def hello(websocket, path):
    id = websocket.request_headers.get("Sec-WebSocket-Key")

    create_snake(id)

    try:
        while True:
            cmd = await websocket.recv()
            # print(f"{id} < {cmd}")
            await websocket.send(process_cmd(cmd, id))
    except Exception as e:
        print(e)
        del players[id]


def nnssd(x, y, dir):
    return [x - {0: 0, 1: 0, 2: 1, 3: -1}[dir], y - {0: 1, 1: -1, 2: 0, 3: 0}[dir]]


def gen_map():
    for i in range(map_size_x):
        for j in range(map_size_y):
            if i == 0 or i == map_size_x - 1 or j == 0 or j == map_size_y - 1:
                blocks.append([i, j, gen_hex_color(rnd_clr(), rnd_clr(), rnd_clr())])


def tick():
    while True:
        time.sleep(0.2)

        for key in players:
            p = players[key]
            p[3].insert(0, [p[0], p[1]])

            p[0] += {0: 0, 1: 0, 2: 1, 3: -1}[p[4]]
            p[1] += {0: 1, 1: -1, 2: 0, 3: 0}[p[4]]

            if len(p[3]) >= 1:
                p[3].pop()


gen_map()

threading.Thread(target=tick).start()

start_server = websockets.serve(hello, "localhost", 9324)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
